package com.humaxdigital.avtovaz.btlinkkeyviewer;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {
    private TextView txtLinkKeyContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtLinkKeyContent = findViewById(R.id.txtLinkKeyContent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (txtLinkKeyContent != null) {
            try {
                String BT_CONFIG_FILE = "/data/misc/bluedroid/bt_config.conf";
                txtLinkKeyContent.setText(loadFileAsString(BT_CONFIG_FILE));
            } catch (Exception ex) {
                txtLinkKeyContent.setText(ex.toString());
            }
        }
    }

    String loadFileAsString(String filename) throws java.io.IOException {
        final int BUFFER_LENGTH = 1024;
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFFER_LENGTH)) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_LENGTH);
            byte[] bytes = new byte[BUFFER_LENGTH];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), StandardCharsets.UTF_8) : new String(baos.toByteArray());
        }
    }
}
